import sys, xbmc

# create playlist
videoList = xbmc.PlayList(xbmc.PLAYLIST_VIDEO)
videoList.clear()

# load playlist
videoList.load("special://profile/playlists/video/Nostalgia.m3u")

# shuffle playlist
videoList.shuffle()

# put playlist on repeat
xbmc.executebuiltin("xbmc.playercontrol(RepeatAll)")

# play playlist
xbmc.Player().play(videoList)